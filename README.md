# Microservicio Catalogos

## Clonar

```sh
$ git clone https://bitbucket.org/amarischile/ms-proplan-catalogos.git
$ cd ms-proplan-catalogos
$ cp .env .env.local
$ npm install
$ npm test
$ npm run start:local
```
> Debes configurar las variables de entorno para tu desarrollo local en .env.local


## Docker

```sh
$ docker build -t ms-proplan-catalogos .

$ docker run -it \
-e APP_PORT=1006 \
-e APP_HOST=0.0.0.0 \
-e APP_PREFIX=/ \
-e APP_HEALTH_PATH=/health \
-e MYSQL_CONNECTION_STRING="mysql://user:pass@server/dbname?debug=true&charset=UTF8_GENERAL_CI" \
-p 1006:1006 --name ms-proplan-catalogos ms-proplan-catalogos
```

## Verificar

Verificar que el servicio este arriba

```sh
$ curl -v http://localhost:1006/health
```