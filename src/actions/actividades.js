import { 
    getActividades, getActividadesPorEspecialidad, 
    getActividad, cambiarEstado, crear, modificar,getTipoActividades
} from  '@repository/actividades';
import { mapToActividades, mapToActividad } from  '@helpers/actividades';

const index = async (req, res, next) => {
    let actividades = [];
    let tipoActividad = null;
    let especialidad = null;
    
    if(req.query["tipoActividad"] !== undefined){
        tipoActividad = parseInt(req.query["tipoActividad"]);
        if(![1,2,3].includes(tipoActividad)){
            tipoActividad = null;
        }
    }
    if(req.query["especialidad"] !== undefined){
        especialidad = parseInt(req.query["especialidad"]);
    }

    if(especialidad == null){
        actividades = mapToActividades(await getActividades(tipoActividad));
    }else{
        actividades = mapToActividades(await getActividadesPorEspecialidad(tipoActividad,especialidad));
    }
    res.status(200).json({
        OK:true, 
        actividades: actividades,
    });      
};

const obtenerPorId = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const actividad = mapToActividad(await getActividad(id));
    res.status(200).json({
        OK:true, 
        actividad: actividad,
    });  
}

const obtenerTipos = async (req, res, next)  => {
    const tipos = await getTipoActividades();
    res.status(200).json({
        OK:true, 
        tipos: tipos,
    });  
}

const dardeBaja = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 0);
    res.status(200).json({
        OK:result, 
    });  
}

const activar = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 1);
    res.status(200).json({
        OK:result, 
    });  
}

const registrar = async (req, res, next)  => {
    const actividad = req.body;
    const result = await crear(actividad);
    res.status(200).json({
        OK:result, 
    });  
}

const actualizar = async (req, res, next)  => {
    const actividad = req.body;
    const id = req.params.id;
    const result = await modificar(id, actividad);
    res.status(200).json({
        OK:result, 
    });  
}



export {
    index,
    obtenerPorId,
    dardeBaja,
    activar,
    registrar,
    actualizar,
    obtenerTipos,
};