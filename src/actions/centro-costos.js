import { 
    getCentroCostos, getCentroCostosPorUsuario, 
    modificarCentroCosto, getCentroCosto, registrarCentroCosto,cambiarEstado,
} from  '@repository/centro-costos';

const index = async (req, res, next) => {
    let usuario = null;
    let centroCostos = [];

    if(req.query["usuario"] !== undefined){
        usuario = parseInt(req.query["usuario"]);
    }

    if(usuario == null){
        centroCostos = await getCentroCostos();
    }else{
        centroCostos = await getCentroCostosPorUsuario(usuario);
    }

    centroCostos = centroCostos.map(cc => {
        return {
            ...cc,
            activo: cc.activo
        }
    })

    res.status(200).json({
        OK:true, 
        centro_costos: centroCostos,
    });      
};


const obtenerPorId = async (req, res, next) => {
    const id = req.query.id;
    let centroCosto = await getCentroCosto(id);
    if(centroCosto !== null){
        centroCosto = {
            ...centroCosto,
            activo: centroCosto.activo
        };
    } 
    res.status(200).json({
        OK: centroCosto ? true : false, 
        centro_costo: centroCosto,
    });      
};

const obtenerPorUser = async (req, res, next) => {
    const id = req.query.id;
    let centroCosto = await getCentroCostosPorUsuario(id);
    if(centroCosto !== null){
        centroCosto = {
            ...centroCosto,
            activo: centroCosto.activo
        };
    }
    res.status(200).json({
        OK: centroCosto ? true : false,
        centro_costos: centroCosto[0],
    });
};
const registrar = async (req, res, next) => {
    try{
        const centroCosto = req.body;
        await registrarCentroCosto(centroCosto);    
        res.status(200).json({
            OK: true, 
        }); 
    }catch(e){
        console.log(e);
        res.status(500).json({
            OK: false, 
            error: e
        });         
    }     
};

const actualizar = async (req, res, next) => {
    try{
        const centroCosto = req.body;
        const id = req.params.id;
        await modificarCentroCosto(id, centroCosto);    
        res.status(200).json({
            OK: true, 
        }); 
    }catch(e){
        console.log(e);
        res.status(500).json({
            OK: false, 
            error: e
        });         
    }        
};

const dardeBaja = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 0);
    res.status(200).json({
        OK:result, 
    });  
}

const activar = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 1);
    res.status(200).json({
        OK:result, 
    });  
}


export {
    index,
    obtenerPorId,
    registrar,
    actualizar,
    dardeBaja,
    activar,
    obtenerPorUser
};
