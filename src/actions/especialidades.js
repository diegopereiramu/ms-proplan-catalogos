import { getEspecialidades, 
    insertEspecialidades, 
    updateEspecialidades,
    deleteEspecialidades,
    getEspecialidadesActive
} from  '@repository/especialidades';

const index = async (req, res, next) => {
    res.status(200).json({
        OK:true, 
        especialidades: await getEspecialidades(),
    });      
};
const active = async (req, res, next) => {
    res.status(200).json({
        OK:true,
        especialidades: await getEspecialidadesActive(),
    });
};
const insertEspecialidad = async (req, res, next) => {
    const id = await insertEspecialidades(req.body);
    if(id == false){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de intentar registrar la especialidad"
        });
    }else{
        res.status(200).json({
            OK:true, 
            id: id
        });
    }   
};
const updateEspecialidad = async (req, res, next) => {
    const id = await updateEspecialidades(req.body, req.params);
    if(id == false){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de intentar actualizar la especialidad"
        });
    }else{
        res.status(200).json({
            OK:true, 
            id: id
        });
    }        
};
const deleteEspecialidad = async (req, res, next) => {
    const id = await deleteEspecialidades(req.body, req.params.id);
    if(id == false){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de intentar actualizar la especialidad"
        });
    }else{
        res.status(200).json({
            OK:true, 
            id: id
        });
    }         
};

export {
    index,
    insertEspecialidad,
    updateEspecialidad,
    deleteEspecialidad,
    active
};
