import { getDate } from  '@repository/utils';

const index = (req, res, next) => {
    res.status(200).json({
        OK:true, 
        message: "home"
    });      
};

const testDb = async (req, res, next) => {
    res.status(200).json({
        OK:true, 
        date: await getDate(),
    });      
};

export {
    index,
    testDb,
};