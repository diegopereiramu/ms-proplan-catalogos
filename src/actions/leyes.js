import { getLeyes, registrarLey, modificarLey, getLey,  cambiarEstado } from  '@repository/leyes';
import { mapToLeyes, mapToLey} from '@helpers/leyes';

const index = async (req, res, next) => {
    const leyes = mapToLeyes(await getLeyes());
    res.status(200).json({
        OK:true, 
        leyes: leyes,
    });      
};

const ley = async (req, res, next) => {
    const id = req.params.id;    
    let _ley = await getLey(id);
    _ley = _ley !== null ? mapToLey(_ley) : null;
    res.status(200).json({
        OK: _ley ? true : false, 
        ley: _ley,
    });      
};

const registrar = async (req, res, next) => {
    try{
        const ley = req.body;
        await registrarLey(ley);    
        res.status(200).json({
            OK: true, 
        }); 
    }catch(e){
        console.log(e);
        res.status(500).json({
            OK: false, 
            error: e
        });         
    }     
};

const modificar = async (req, res, next) => {
    try{
        const ley = req.body;
        const id = req.params.id;
        await modificarLey(id, ley);    
        res.status(200).json({
            OK: true, 
        }); 
    }catch(e){
        console.log(e);
        res.status(500).json({
            OK: false, 
            error: e
        });         
    }        
};

const dardeBaja = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 0);
    res.status(200).json({
        OK:result, 
    });  
}

const activar = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 1);
    res.status(200).json({
        OK:result, 
    });  
}


export {
    dardeBaja,
    activar,
    index,
    registrar,
    modificar,
    ley,
};