import { getPeriodos } from  '@repository/periodos';
import { mapPeriodos } from  '@helpers/periodos';

const index = async (req, res, next) => {
    const periodos = mapPeriodos(await getPeriodos());
    res.status(200).json({
        OK:true, 
        periodos: periodos,
    });      
};


export {
    index,
};