import {getRoles} from "../repository/rol";

const obtenerRoles = async (req,res,next) => {
    let roles = [];
    roles = await getRoles();
    res.status(200).json({
        OK: true,
        data: roles.rows
    });
};
export {
    obtenerRoles,
};


