import { getTitulos } from '@repository/titulos';

const index = async (req, res, next) => {
    try {
        const titulos = await getTitulos();
        res.status(200).json({
            OK: true,
            titulos: titulos,
        });
    } catch (error) {
        res.status(200).json({
            OK: false,
            titulos: [],
        });
    }

};


export {
    index,
};