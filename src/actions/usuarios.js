import { mapToUsuarios, mapToUsuario} from '../helpers/usuarios';
import {
    getUsuarios, getUsuario, getUsuariosCentroCostos,
    getUsuariosRoles, cambiarEstado, crearUsuario,actualizarUsuario
} from '../repository/usuarios';

const obtenerUsuarios = async (req,res,next) => {
    let usuarios = [];
    usuarios = await getUsuarios();
    res.status(200).json({
        OK: true,
        data: usuarios.rows
    });
};



const index = async (req, res, next) => {
    try {
        const usuarios = await getUsuarios();
        const centroCostos = await getUsuariosCentroCostos();
        const roles = await getUsuariosRoles();
        const usuariosMap = mapToUsuarios(usuarios, centroCostos, roles);
        res.status(200).json({
            OK: true,
            usuarios: usuariosMap
        });
    } catch (error) {
        res.status(200).json({
            OK: false,
            usuarios: [],
        });
    }
};

const obtenerPorId = async (req, res, next) => {
    try {
        const id = parseInt(req.params.id);
        const usuario = await getUsuario(id);
        console.log(usuario);
        let usuarioMap = null;
        if(usuario !== null){
            const centroCostos = await getUsuariosCentroCostos(id);
            const roles = await getUsuariosRoles(id);
            usuarioMap = mapToUsuario(usuario, centroCostos, roles);
        }
        res.status(200).json({
            OK: true,
            usuario: usuarioMap
        });
    } catch (error) {
        console.log(error);
        res.status(200).json({
            OK: false,
            usuario: null,
        });
    }
};

const dardeBaja = async (req, res, next)  => {
    const id = req.body;
    const result = await cambiarEstado(id);
    res.status(200).json({
        OK:result, 
    });  
}

const activar = async (req, res, next)  => {
    const id = parseInt(req.params.id);
    const result = await cambiarEstado(id, 1);
    res.status(200).json({
        OK:result, 
    });  
}

const postUser = async (req, res, next) => {
    const nuevoRegistro = req.body;
    const usuario = await crearUsuario(nuevoRegistro);
    res.status(200).json({
        OK: true,
        data: usuario
    });
};

const putUser = async (req, res, next) => {
    const nuevoRegistro = req.body;
    const usuario = await actualizarUsuario(nuevoRegistro);
    res.status(200).json({
        OK: true,
        data: usuario
    });
};
export {
    index,
    obtenerPorId,
    dardeBaja,
    activar,
    obtenerUsuarios,
    putUser,
    postUser,
};
