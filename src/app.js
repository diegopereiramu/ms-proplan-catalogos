import api from '@amacl/roboc-api';
import { apiOptions } from '@config';
import home from '@routes/home';
import especialidades from '@routes/especialidades';
import actividades from '@routes/actividades';
import centroCostos from '@routes/centro-costos';
import leyes from '@routes/leyes';
import periodos from '@routes/periodos';
import titulos from '@routes/titulos';
import usuarios from '@routes/usuarios';
import rol from "./routes/rol";

const run = () => {
    api(apiOptions,(router, app) => {
        home(router);
        especialidades(router);
        actividades(router);
        centroCostos(router);
        periodos(router);
        leyes(router);
        titulos(router);
        usuarios(router);
        rol(router);
        return router;
    });
};

export {
    run,
}
