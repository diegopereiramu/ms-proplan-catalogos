const mapToActividades = (actividades) => {
    return actividades.map(actividad => {
        return mapToActividad(actividad);
    });
};

const mapToActividad = (actividad) => {

    if(actividad === null){
        return null;
    }

    return {
        id: actividad.id,
        codigo: actividad.codigo,
        descripcion: actividad.descripcion,
        rendimiento: {
            exige: actividad.rendimiento,
            horas: actividad.horas_rendimiento
        },
        tipo_actividad: {
            id: actividad.tipo_actividad_id,
            codigo: actividad.tipo_actividad_codigo,
            descripcion: actividad.tipo_actividad_descripcion,
        },
        activo: actividad.activo
    };
}

export {
    mapToActividades,
    mapToActividad,
};