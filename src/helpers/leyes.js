const mapToLey = (ley) => {
    return {
        id: ley.id,
        codigo: ley.codigo,
        descripcion: ley.descripcion,
        distribucion_horas: ley.distribucion_horas,
        activo: ley.activo
    };
};

const mapToLeyes = (leyes) => {
    return leyes.map(ley => {
        return mapToLey(ley);
    });
};


export {
    mapToLeyes,
    mapToLey,
};