const moment = require('moment');

const mapPeriodos = (periodos) => {
    return periodos.map(periodo => {
        return {
            id: periodo.id,
            fecha: {
                inicio: moment(periodo.fecha_inicio).format('DD/MM/YYYY'),
                termino: moment(periodo.fecha_termino).format('DD/MM/YYYY'),
            },
            actual: periodo.actual == 1,
            cerrado: periodo.cerrado == 1


        };
    });
};

export {
    mapPeriodos
};