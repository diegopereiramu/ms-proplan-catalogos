export const mapToUsuario = (usuario, centroCostos, roles) => {
    return {
        id: usuario.id,
        usuario: usuario.usuario,
        nombre: usuario.nombre,
        activo: usuario.activo == 1,
        centro_costos: centroCostos.map(cc => {
            return {
                id: cc.id,
                codigo: cc.codigo,
                descripcion: cc.descripcion,
            }
        }),
        roles: roles.map(r => {
            return {
                id: r.id,
                codigo: r.codigo,
                descripcion: r.descripcion,
            }
        }),
    };
}

export const mapToUsuarios = (usuarios, centroCostos, roles) => {
    const list = [];
    let centroCostosUsuario = [];
    let rolesUsuario = [];
    usuarios.forEach(usuario => {
        centroCostosUsuario = centroCostos.filter(cc => cc.usuario_id == usuario.id);
        rolesUsuario = roles.filter(r => r.usuario_id == usuario.id);
        list.push(mapToUsuario(usuario, centroCostosUsuario, rolesUsuario))
    });
    return list;
};