import { query } from './pg.connect';

const getActividades = async (tipoActividad) => {
    let params = [];
    let actividades = [];
    let q = `
      select a.id, a.codigo, a.descripcion, a.rendimiento, a.horas_rendimiento, a.activo, 
      ta.id tipo_actividad_id,
      ta.codigo tipo_actividad_codigo, 
      ta.descripcion tipo_actividad_descripcion
      from "proplan"."actividad" a
      inner join "proplan"."tipo_actividad" ta  on ta.id = a.tipo_actividad_id
    `;
    if(tipoActividad != null){
        q = q + `where a.tipo_actividad_id = $1`;
        params.push(tipoActividad);
    }
    try{
        actividades = await query(q, params);
    }catch(e){
        console.log(e);
    }

    return actividades.rows;
};

const getTipoActividades = async () => {
    let tipos = [];
    const q = `
        SELECT id, codigo, descripcion FROM "proplan"."tipo_actividad" 
    `;
    try{
        tipos = await query(q, []);
    }catch(e){
        console.log(e);
    }

    return tipos.rows;
};

const crear = async (actividad) => {
    const q = `
        INSERT INTO "proplan"."actividad" (codigo, tipo_actividad_id, descripcion, rendimiento, horas_rendimiento) 
        VALUES ($1, $2, $3, $4, $5);
    `;
    try{
        await query(q, [
            actividad.codigo,
            actividad.tipo_actividad,
            actividad.descripcion,
            actividad.rendimiento ? 1 : 0,
            actividad.horas_rendimiento,
        ]);

        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};

const modificar = async (id, actividad) => {
    const q = `
        update "proplan"."actividad" 
        set codigo = $1, 
        tipo_actividad_id = $2, 
        descripcion = $3, 
        rendimiento = $4, 
        horas_rendimiento = $5
        WHERE id = $6
    `;
    try{
        await query(q, [
            actividad.codigo,
            actividad.tipo_actividad,
            actividad.descripcion,
            actividad.rendimiento ? 1 : 0,
            actividad.horas_rendimiento,
            id,
        ]);

        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};


const getActividad = async (id) =>{
    let actividades = [];
    const q = `
      select a.id, a.codigo, a.descripcion, a.rendimiento, a.horas_rendimiento, a.activo, 
      ta.id tipo_actividad_id,
      ta.codigo tipo_actividad_codigo, 
      ta.descripcion tipo_actividad_descripcion
      from "proplan"."actividad" a
      inner join "proplan".tipo_actividad" ta  on ta.id = a.tipo_actividad_id
      where a.id = $1
    `;
    try{
        actividades = await query(q, [id]);
    }catch(e){
        console.log(e);
    }

    return actividades.rows.length > 0 ? actividades.rows[0] : null;
};

const cambiarEstado = async (id, estado) =>{
    const q = `
      update "proplan"."actividad" a
      set activo = $1
      where a.id = $2
    `;
    try{
        await query(q, [estado, id]);
      return true;
    }catch(e){
        console.log(e);
    }

    return false;
};



const getActividadesPorEspecialidad = async (tipoActividad, especialidad) => {
    let actividades = [];
    const q = `
        select * 
            from (
            select a.id, a.codigo, a.descripcion, a.rendimiento, a.horas_rendimiento,
            ta.id tipo_actividad_id,
            ta.codigo tipo_actividad_codigo, 
            ta.descripcion tipo_actividad_descripcion
            from "proplan"."actividad" a
            inner join "proplan"."tipo_actividad" ta  on ta.id = a.tipo_actividad_id
            inner join "proplan"."actividad_especialidad" ae on ae.especialidad_id = $1
            and ae.actividad_id = a.id
            union
            select a.id, a.codigo, a.descripcion, a.rendimiento, a.horas_rendimiento,
            ta.id tipo_actividad_id,
            ta.codigo tipo_actividad_codigo, 
            ta.descripcion tipo_actividad_descripcion
            from "proplan"."actividad" a
            inner join "proplan"."tipo_actividad" ta  on ta.id = a.tipo_actividad_id
            where a.id not in(
                select actividad_id 
                from "proplan"."actividad_especialidad"
                where actividad_id = a.id 
            )
        ) view
        where tipo_actividad_id is null or view.tipo_actividad_id = $2 or view.tipo_actividad_id = 3 

    `;
    try{
        actividades = await query(q,[especialidad, tipoActividad]);
    }catch(e){
        console.log(e);
    }
    return actividades.rows;
};

export {
    getActividad,
    getActividades,
    getActividadesPorEspecialidad,
    cambiarEstado,
    crear,
    modificar,
    getTipoActividades,
}
