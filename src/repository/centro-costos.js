import { query } from './pg.connect';

const getCentroCostos = async () => {
    let centroCostos = [];
    const q = `
      select id, codigo, descripcion, activo
      from "proplan"."centro_costo";
    `;
    try{
        centroCostos = await query(q);
    }catch(e){
        console.log(e);
    }

    return centroCostos.rows;
};


const getCentroCosto = async (id) => {
    let centroCostos = [];
    const q = `
      select id, codigo, descripcion, activo
      from "proplan"."centro_costo"
      where id = $1;
    `;
    try{
        centroCostos = await query(q,[id]);
    }catch(e){
        console.log(e);
    }

    return centroCostos.rows.length > 0 ? centroCostos.rows[0] : null;
};

const registrarCentroCosto = async (centroCosto) => {
    const q = `
      insert into "proplan"."centro_costo" (codigo, descripcion)
      values ($1, $2)
    `;
    try{
        await query(q,[centroCosto.codigo, centroCosto.descripcion]);
        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};

const modificarCentroCosto = async (id, centroCosto) => {
    const q = `
      update "proplan"."centro_costo" 
      set codigo = $1, descripcion = $2
      where id = $3
    `;
    try{
        await query(q,[centroCosto.codigo, centroCosto.descripcion, id]);
        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};

const cambiarEstado = async (id, estado) => {
    const q = `
      update "proplan"."centro_costo" 
      set activo = $1
      where id = $2
    `;
    try{
        await query(q,[estado, id]);
        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};

const getCentroCostosPorUsuario = async (usuario) => {
    let centroCostos = [];
    const q = `
      select cc.id, cc.codigo, cc.descripcion
      from "proplan"."usuario_centro_costo" ucc
      inner join "proplan"."centro_costo" cc on cc.id = ucc.centro_costo_id
      where ucc.usuario_id = $1
    `;
    try{
        centroCostos = await query(q,[usuario]);
    }catch(e){
        console.log(e);
    }

    return centroCostos.rows;
};


export {
    getCentroCostos,
    getCentroCosto,
    getCentroCostosPorUsuario,
    registrarCentroCosto,
    modificarCentroCosto,
    cambiarEstado,
};
