import { query } from './pg.connect';

const getEspecialidades = async () => {
    let especialidades = [];
    const q = `
      select id, codigo, descripcion, estado
      from "proplan"."especialidad";
    `;
    try {
        especialidades = await query(q);
    } catch (e) {
        console.log(e);
    }

    return especialidades.rows;
};
const getEspecialidadesActive = async () => {
    let especialidades = [];
    const q = `
      select id, codigo, descripcion, estado
      from "proplan"."especialidad" where estado=true;
    `;
    try {
        especialidades = await query(q);
    } catch (e) {
        console.log(e);
    }

    return especialidades.rows;
};

const insertEspecialidades = async (especialidad) => {
    const q = `
        INSERT INTO "proplan"."especialidad"(descripcion, codigo)
        values($1,$2);
    `;
    try {
        const result = await query(q, [
            especialidad.descripcion,
            especialidad.codigo,            
        ]);

        return true
    } catch (e) {
        console.log(e);
    }

    return false;
};

const updateEspecialidades = async (especialidad, id) => {
    const q = `
        UPDATE "proplan"."especialidad"
        SET
            descripcion = $1
        WHERE id = $2;
    `;

    try {
        const result = await query(q, [
            especialidad.descripcion,
            id.id,
        ]);

        return true
    } catch (e) {
        console.log(e);
    }

    return false;
};

const deleteEspecialidades = async (especialidad, id) => {
    const q = `
        UPDATE "proplan"."especialidad"
        set
            estado = $1
        where id = $2;
    `;

    try {
        const result = await query(q, [
            especialidad.estado,
            id,
        ]);

        return true
    } catch (e) {
        console.log(e);
    }
    return false;
};
export {
    getEspecialidades,
    insertEspecialidades,
    updateEspecialidades,
    deleteEspecialidades,
    getEspecialidadesActive
}
