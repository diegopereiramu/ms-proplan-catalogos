import { query } from './pg.connect';

const getLeyes = async () => {
    let leyes = [];
    const q = `
      select id, codigo, descripcion, distribucion_horas, activo
      from "proplan"."ley";
    `;
    try{
        leyes = await query(q);
    }catch(e){
        console.log(e);
    }

    return leyes.rows;
};

const getLey = async (id) => {
    let ley = null;
    const q = `
      select id, codigo, descripcion, distribucion_horas, activo
      from "proplan"."ley"
      where id = $1
    `;
    try{
        ley = await query(q,[id]);
        ley = ley.length > 0 ? ley[0] : null;
    }catch(e){
        console.log(e);
    }

    return ley.rows;
};

const registrarLey = async (ley) => {
    const q = `
        INSERT INTO "proplan"."ley" (codigo, descripcion, distribucion_horas) 
        VALUES ($1,$2,$3);
    `;    
    try{
        await query(q, [ley.codigo, ley.descripcion, ley.distribucion_horas]);
    }catch(e){
        throw e;
    }

    return true;
};

const modificarLey = async (id, ley) => {
    const q = `
        UPDATE "proplan"."ley" 
        SET codigo = $1, descripcion = $2, distribucion_horas = $3
        WHERE id = $4
    `;    
    try{
        await query(q, [ley.codigo, ley.descripcion, ley.distribucion_horas, id]);
    }catch(e){
        throw e;
    }

    return true;
};

const cambiarEstado = async (id, estado) =>{
    const q = `
      update "proplan"."ley" a
      set activo = $1
      where a.id = $2
    `;
    try{
        await query(q, [estado, id]);
        return true;
    }catch(e){
        console.log(e);
    }

    return false;
};


export {
    cambiarEstado,
    getLeyes,
    getLey,
    registrarLey,
    modificarLey,
}
