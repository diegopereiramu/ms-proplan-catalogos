import { query } from './pg.connect';

const getPeriodos = async () => {
    let periodos = [];
    const q = `
      select 
        id, 
        fecha_inicio,
        fecha_termino,
        case 
          when now() between fecha_inicio and fecha_termino then 1
          else 0
        end actual,
        case 
          when now() > fecha_termino then 1
        end cerrado
      from "proplan"."periodo"
      limit 3;
    `;
    try{
        periodos = await query(q);
    }catch(e){
        console.log(e);
    }
    return periodos.rows;
};


export {
    getPeriodos,
}
