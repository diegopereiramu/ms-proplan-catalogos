import { query } from './pg.connect';

const getRoles = async () => {
    let usuarios = [];
    const q = `
        SELECT id, codigo
        FROM "proplan"."rol";
    `;
    try{
        usuarios = await query(q);
    }catch(e){
        console.log(e);
    }

    return usuarios;
};



export {
    getRoles,

}
