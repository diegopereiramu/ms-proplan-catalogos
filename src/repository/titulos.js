import { query } from './pg.connect';

const getTitulos = async () => {
    let titulos = [];
    const q = `
      select id, descripcion
      from "proplan"."titulo";
    `;
    try{
        titulos = await query(q);
    }catch(e){
        console.log(e);
    }

    return titulos.rows;
};


export {
    getTitulos,
}
