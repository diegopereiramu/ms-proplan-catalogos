import { query } from './pg.connect';

const getUsuarios = async () => {
    let usuarios = [];
    const q = `
        SELECT u.id, u.nombre, u.usuario, u.activo, r.descripcion, cc.descripcion as centro_costo
        FROM "proplan"."usuario" u 
        inner join "proplan"."usuario_rol" ur on u.id = ur.usuario_id 
        inner join "proplan"."rol" r on r.id = ur.rol_id
        inner join "proplan"."usuario_centro_costo" ucc on  u.id = ucc.usuario_id
        inner join "proplan"."centro_costo" cc on ucc.centro_costo_id= cc.id;
    `;
    try{
        usuarios = await query(q);
    }catch(e){
        console.log(e);
    }

    return usuarios;
};

const getUsuario = async (id) => {
    let usuarios = [];
    const q = `
        SELECT id, nombre, usuario, activo
        FROM "proplan"."usuario"
        WHERE id = $1
        ;
    `;
    try{
        usuarios = await query(q, [id]);
    }catch(e){
        console.log(e);
    }

    return usuarios.length > 0 ? usuarios[0] : null;
};

const getUsuariosCentroCostos = async (idUsuario) => {
    let centroCostos = [];
    const q = `
        SELECT ucc.id, ucc.usuario_id, ucc.centro_costo_id, cc.codigo, cc.descripcion
        FROM "proplan"."usuario_centro_costo" ucc
        INNER JOIN "proplan"."centro_costo" cc on cc.id = ucc.centro_costo_id
        WHERE ucc.usuario_id = ? or 1 = ?;
    `;
    try{
        centroCostos = await query(q,[idUsuario, idUsuario == 0 ? 1 : 0]);
    }catch(e){
        console.log(e);
    }

    return centroCostos;
};

const getUsuariosRoles = async (idUsuario = 0) => {
    let roles = [];
    const q = `
        SELECT ur.id, ur.usuario_id, ur.rol_id, r.codigo, r.descripcion
        FROM "proplan"."usuario_rol" ur
        INNER JOIN "proplan"."rol" r on r.id	 = ur.rol_id
        WHERE ur.usuario_id = $1 or 1 = $2;
    `;
    try{
        roles = await query(q,[idUsuario, idUsuario == 0 ? 1 : 0]);
    }catch(e){
        console.log(e);
    }

    return roles.rows;
};

const cambiarEstado = async (n) => {
    if(n.activo)
    {
        const q = `
      update "proplan"."usuario" 
      set activo = $1
      where id = $2
    `;
    try{
        await query(q,[false, n.id]);
        return true;
    }catch(e){
        console.log(e);
    }

    return false;
    }
else
    {
        const q = `
      update "proplan"."usuario" 
      set activo = $1
      where id = $2
    `;
        try{
            await query(q,[true, n.id]);
            return true;
        }catch(e){
            console.log(e);
        }

        return false;
    }
};
const crearUsuario=async(n) => {
    let q ='INSERT INTO "proplan"."usuario" (nombre, usuario, activo) values' +
        '($1, $2, $3)';
    try{
        await query(q, [
            n.nombre,
            n.usuario,
            true,
        ]);
        q ='insert into "proplan"."usuario_rol"(usuario_id,rol_id) values ((select id from "proplan"."usuario" where usuario=$1),$2)'
        try{
            await query(q, [
                n.usuario,
                n.rol
            ]);
            q ='insert into "proplan"."usuario_centro_costo"(usuario_id,centro_costo_id) values ((select id from "proplan"."usuario" where usuario=$1),$2)'
            try{
                await query(q, [
                    n.usuario,
                    n.centro_costo
                ]);
                return true;
            }catch(e){
                console.log(e);
            }
        }catch(e){
            console.log(e);
        }
    }catch(e){
        console.log(e);
    }
    return false;
}

const actualizarUsuario=async(n) => {
    let q ='update "proplan"."usuario" set nombre=$1, usuario=$2, activo=$3'+
    ' where id=$4';
    try{
        await query(q, [
            n.nombre,
            n.usuario,
            n.activo,
            n.id
        ]);
        q ='update "proplan"."usuario_rol" set rol_id=$2 where usuario_id=$1'
        try{
            await query(q, [
                n.id,
                n.rol,
            ]);
            return true;
        }catch(e){
            console.log(e);
        }
    }catch(e){
        console.log(e);
    }
    return false;
}
export {
    getUsuario,
    getUsuarios,
    getUsuariosCentroCostos,
    getUsuariosRoles,
    cambiarEstado,
    crearUsuario,
    actualizarUsuario
}
