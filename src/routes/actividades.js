import { index, obtenerPorId, dardeBaja,activar, registrar, actualizar, obtenerTipos } from '@actions/actividades';

export default (router, app) => {
    router.get('/actividades', index);
    router.get('/actividades/tipos', obtenerTipos);    
    router.get('/actividades/:id', obtenerPorId);
    router.post('/actividades', registrar);
    router.put('/actividades/:id', actualizar);
    router.delete('/actividades/:id',dardeBaja);
    router.put('/actividades/:id/activar',activar);        
};