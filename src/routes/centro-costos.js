import { index, obtenerPorId, registrar, actualizar, dardeBaja, activar,obtenerPorUser } from '@actions/centro-costos';

export default (router, app) => {
    router.get('/centro-costos', index);
    router.get('/centro-costo/', obtenerPorUser);
    router.post('/centro-costos', registrar);
    router.put('/centro-costos/:id', actualizar);
    router.delete('/centro-costos/:id',dardeBaja);
    router.put('/centro-costos/:id/activar',activar);           
};
