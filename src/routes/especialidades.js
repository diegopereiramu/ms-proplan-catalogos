import { index,active } from '@actions/especialidades';
import { insertEspecialidad } from '@actions/especialidades';
import { updateEspecialidad } from '@actions/especialidades';
import { deleteEspecialidad } from '@actions/especialidades';


export default (router, app) => {
    router.get('/especialidades', index);
    router.get('/especialidadesActive', active);
    router.post('/especialidades', insertEspecialidad);
    router.put('/especialidades/:id', updateEspecialidad);
    router.put('/change-status-especialidad/:id', deleteEspecialidad);
};
