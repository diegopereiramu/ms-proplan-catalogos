import { index, testDb } from '@actions/home';

export default (router, app) => {
    router.get('/', index);
    router.get('/test-db', testDb);
};