import { index, registrar, modificar, ley, dardeBaja, activar} from '@actions/leyes';

export default (router, app) => {
    router.get('/leyes', index);
    router.get('/leyes/:id', ley);
    router.post('/leyes', registrar);
    router.put('/leyes/:id', modificar);
    router.delete('/leyes/:id',dardeBaja);
    router.put('/leyes/:id/activar',activar);            
};