import { index } from '@actions/periodos';

export default (router, app) => {
    router.get('/periodos', index);
};