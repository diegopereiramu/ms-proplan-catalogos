import {obtenerRoles} from "../actions/rol";

export default (router, app) => {
    router.get('/roles', obtenerRoles);
};
