import { index } from '@actions/titulo';

export default (router, app) => {
    router.get('/titulos', index);
};