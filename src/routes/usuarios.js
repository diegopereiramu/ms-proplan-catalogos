import { index, obtenerPorId, dardeBaja, activar,obtenerUsuarios,postUser,putUser } from '@actions/usuarios';

export default (router, app) => {
    router.get('/usuarios', index);
    router.get('/usuarios/:id', obtenerPorId);
    router.get('/usuariosFull',obtenerUsuarios);
    router.post('/usuariosFull', postUser);
    router.put('/usuarioFull/', putUser);
    router.put('/usuariosFull',dardeBaja);
    router.put('/usuarios/:id/activar',activar);           
};
